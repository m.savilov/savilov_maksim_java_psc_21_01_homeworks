package msavilov;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner =  new Scanner(System.in);

        TV tv = new TV("LG");
        Channel channel1 = new Channel("Первый");
        Channel channel2 = new Channel("Россия");
        Channel channel3 = new Channel("Матч ТВ");

        tv.addChannel(channel1);
        tv.addChannel(channel2);
        tv.addChannel(channel3);

        Broadcasts broadcast1 = new Broadcasts("Олимпиада");
        Broadcasts broadcast2 = new Broadcasts("Что? Где? Когда?");
        Broadcasts broadcast3 = new Broadcasts("КВН");
        Broadcasts broadcast4 = new Broadcasts(broadcast1);
        Broadcasts broadcast5 = new Broadcasts("Вести");
        Broadcasts broadcast6 = new Broadcasts("60 минут");
        Broadcasts broadcast7 = new Broadcasts(broadcast1);
        Broadcasts broadcast8 = new Broadcasts("Бокс");
        Broadcasts broadcast9 = new Broadcasts("Футбол");

        channel1.addProgram(broadcast1);
        channel1.addProgram(broadcast2);
        channel1.addProgram(broadcast3);
        channel2.addProgram(broadcast4);
        channel2.addProgram(broadcast5);
        channel2.addProgram(broadcast6);
        channel3.addProgram(broadcast7);
        channel3.addProgram(broadcast8);
        channel3.addProgram(broadcast9);

        Controller controller = new Controller(tv);

        controller.startPrint();
        int numChannel = scanner.nextInt();
        controller.turnOn(numChannel);
    }

}
