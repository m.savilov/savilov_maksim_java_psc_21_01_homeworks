package msavilov;

public class Broadcasts {
    private String broadcastName;

    public Broadcasts(String nameProgram) {
        this.broadcastName = nameProgram;
    }
    public Broadcasts(Broadcasts orig) {
        this.broadcastName = orig.broadcastName;
    }

    public String getNameBroadcast() {
        return broadcastName;
    }
}
