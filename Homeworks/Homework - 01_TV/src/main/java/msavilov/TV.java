package msavilov;

public class TV {
    private static final int MAX_SIZE = 4;

    private String modelName;
    private int channelNumber;
    private Channel[] channels;

    public TV(String modelName) {
        this.modelName = modelName;
        this.channelNumber = 0;
        this.channels = new Channel[MAX_SIZE];
    }

    public void addChannel(Channel name) {
        channelNumber++;
        channels[channelNumber] = name;
    }

    public String getNameTv() {
        return modelName;
    }

    public int getCountChannel() {
        return channelNumber;
    }

    public String getChannelName(int number) {
        return channels[number].getNameChannel();
    }

    public String getBroadcastName(int numChannel, int numBroadcast) {
        return channels[numChannel].getBroadcastName(numBroadcast);
    }
}

