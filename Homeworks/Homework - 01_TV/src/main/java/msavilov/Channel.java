package msavilov;

public class Channel {
    private static final int MAX_SIZE = 3;

    private String nameChannel;
    private int broadcastCount;
    private Broadcasts[] broadcasts;

    public Channel(String nameChannel) {
        this.nameChannel = nameChannel;
        this.broadcastCount = 0;
        this.broadcasts = new Broadcasts[MAX_SIZE];
    }

    public void addProgram(Broadcasts name) {
        broadcasts[broadcastCount] = name;
        broadcastCount++;
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public String getBroadcastName (int broadcastNumber) {
        return broadcasts[broadcastNumber].getNameBroadcast();
    }
}
