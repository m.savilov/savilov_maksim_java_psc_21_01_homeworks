package msavilov;

import java.util.Random;

public class Controller {
    private final TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void startPrint() {
        System.out.println("Вас приветствует телевизор: " + tv.getNameTv());
        System.out.print("Выберите канал: ");
    }

    public void errorPrint (int numChannel) {
        System.out.println("Канал " + numChannel + " не настроен!");
    }

    /** Метод симулирует работу телевизора с выбором канала. ОН принимает на вход номер канала на телевизоре.
     * Если он существует, то случайно выбирается одна из потенциальных телевизионных программ на этом канале.
     * Если такого канала нет, то выдаётся ошибка
     * @param numChannel - соответствует каналу на телевизоре.
     */
    public void turnOn(int numChannel) {
        if (numChannel > 0 && numChannel <= tv.getCountChannel()) {
            Random random = new Random();
            int broadCastNumber = random.nextInt(2) + 1;
            System.out.println("Вы смотрите канал " + "\"" + tv.getChannelName(numChannel) + "\"");
            System.out.println("Сейчас идёт " + "\"" + tv.getBroadcastName(numChannel, broadCastNumber) + "\"");
        } else {
            errorPrint(numChannel);
        }
    }

}
